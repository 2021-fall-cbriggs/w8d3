package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W8d3Application {

	public static void main(String[] args) {
		SpringApplication.run(W8d3Application.class, args);
	}

}
