package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.service.CarpartsService;

@Controller
public class CarpartsMgrController {
    @Autowired
    CarpartsService carpartsService;
        
    @GetMapping(value="/carpartsmgr")
    public String getCarpartMgr(Model model) {
        return "carpartsmgr";
    }
    
    @ResponseBody
    @GetMapping(value="/carpartsmgr/getcarparts")
    public List<Carpart> getCarpartsData() {
        return carpartsService.getCarparts();
    }
    
    @GetMapping(value="/carpartsmgr/data")
    public String getCarpartsMgrData(Model model) throws Exception {
        List<Carpart> carpartList = carpartsService.getCarparts();
        ObjectMapper mapper = new ObjectMapper();
        String carpartsListJson = mapper.writeValueAsString(carpartList);
        model.addAttribute("carpartsListJson", carpartsListJson);
        model.addAttribute("active", "carpartsmgrdata");
        return "carpartsmgrdata";
    }
    
    @GetMapping(value="/carpartsmgr/fetch")
    public String getCarpartsMgrFetch(Model model) throws Exception {
        return "carpartsmgrfetch";
    }
}
