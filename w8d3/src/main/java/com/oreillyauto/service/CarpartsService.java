package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Carpart;
//import com.oreillyauto.dto.Email;

public interface CarpartsService {
    public Carpart getCarpartByPartNumber(String partNumber) throws Exception;
	public List<Carpart> getCarparts();
	public Carpart saveCarpart(Carpart carpart);
	public void deleteCarpartByPartNumber(Carpart carpart);
	//public void sendEmail(Email email) throws Exception;
    public Carpart addUpdateCarpart(String partnumber, String title);
}
