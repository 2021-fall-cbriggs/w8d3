<%@ include file="/WEB-INF/layouts/include.jsp" %>

<nav class="navbar bg-light">
	<ul class="nav nav-pills flex-column">
		<li>
			<a class="${active eq 'carpartsmgr' ? 'active nav-link' : 'nav-link'}"
						href="<c:url value='/carpartsmgr'/>">
				Carparts Manager URL
			</a>
		</li>
		<li>
			<a class="${active eq 'carpartsmgrdata' ? 'active nav-link' : 'nav-link'}"
						href="<c:url value='/carpartsmgr/data'/>">
				Carparts Manager Data
			</a>
		</li>
		<li>
			<a class="${active eq 'carpartsmgrfetch' ? 'active nav-link' : 'nav-link'}"
						href="<c:url value='/carpartsmgr/fetch'/>">
				Carparts Manager Fetch
			</a>
		</li>
	</ul>
</nav>