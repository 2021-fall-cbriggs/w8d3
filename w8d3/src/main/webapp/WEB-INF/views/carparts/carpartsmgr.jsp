<%@ include file="/WEB-INF/layouts/include.jsp" %>

<h1>Carparts Manager</h1>

<orly-table id="carpartsTable" loaddataoncreate url="<c:url value='/carpartsmgr/getcarparts '/>" includefilter
				bordered maxrows="10" tabletitle="Search Results" class="invisible">
	<orly-column field="" label="Action" class=""></orly-column>
	<orly-column field="partNumber" label="Part Number" class="" sorttype="natural"></orly-column>
	<orly-column field="title" label="Title"></orly-column>
	<orly-column field="line" label="Line"></orly-column>
	<orly-column field="description" label="Description"></orly-column>
</orly-table>