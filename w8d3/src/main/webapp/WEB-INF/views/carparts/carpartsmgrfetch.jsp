<%@ include file="/WEB-INF/layouts/include.jsp" %>

<h1>Carparts Manager Fetch</h1>
<orly-btn id="reloadBtn" spinonclick text="Reload" btntype="success"></orly-btn>
<orly-table id="carpartsTable" loaddataoncreate url="<c:url value='/carpartsmgr/getcarparts'/>" includefilter
				bordered maxrows="10" tabletitle="Search Results" class="invisible">
	<orly-column field="" label="Action" class=""></orly-column>
	<orly-column field="partNumber" label="Part Number" class="" sorttype="natural"></orly-column>
	<orly-column field="title" label="Title"></orly-column>
	<orly-column field="line" label="Line"></orly-column>
	<orly-column field="description" label="Description"></orly-column>
</orly-table>

<script>
	var carpartsTable, reloadBtn;
	orly.ready.then(()=>{
		carpartsTable = orly.qid("carpartsTable");
		reloadBtn = orly.qid("reloadBtn");
		
		addEventListeners();
	});
	
	function addEventListeners() {
		reloadBtn.addEventListener("click", ()=>{
			reloadTableData();
		});	
	}
	
	function reloadTableData() {
		fetch("<c:url value='/carpartsmgr/getcarparts' />")
		.then(httpresponseservlet => {
			if(httpresponseservlet.ok) {
				return httpresponseservlet.json();
			} else {
				alert("NO!!!!!!!! Bad Http Status: " + httpresonseservlet.status);
			}
		})
		.then(tableData => {
			carpartsTable.data = tableData;
		})
		.catch(error => {
			alert("NO!!!!! Error = " + error);
		})
		.finally(()=> {
			reloadBtn.stopSpinning();
		});
	}
</script>